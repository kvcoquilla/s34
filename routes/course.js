const express = require('express')
const router = express.Router()
const courseController = require('../controllers/course')
const auth = require('../auth');

router.post('/', auth.verify, (req,res) => {

	const userData = auth.decode(req.headers.authorization)

	courseController.addCourse(req.body,userData).then(resultFromController => res.send(resultFromController))
})

module.exports = router;