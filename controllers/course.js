const Course = require('../models/Course')
const auth = require('../auth');

module.exports.addCourse = (reqBody, payload) => {

	if (payload.isAdmin == false) {

		console.log(payload);
		return "Not authorized to add course"

	}

	else {
		
		console.log(payload);

		let newCourse =  new Course ({

			name : reqBody.name,
			description: reqBody.description,
			price : reqBody.price

		})

		return newCourse.save().then((course, error) => {
			if(error) {
				return false
			} else {
				return "Course was added"
			}
		})
	}
}

